// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('todo', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.factory('Projects', function($http) {
  return {
    all: function() {
      return $http({
        method: 'GET',
        // Should be localhost if browser, but 10.0.2.2 is the emulator connection with the local machine
        url: 'http://10.0.2.2:8081/projects' 
      });
    },
    save: function(project) {
      return $http({
        method: 'POST',
        // Should be localhost if browser, but 10.0.2.2 is the emulator connection with the local machine
        url: 'http://10.0.2.2:8081/projects',
        data: project
      });
    },
    newProject: function(projectTitle) {
      return {
        title: projectTitle,
        tasks: []
      };
    }
  }
})

.controller('TodoCtrl', function($scope, $timeout, $ionicModal, Projects, $ionicSideMenuDelegate) {

  var createProject = function(projectTitle) {
    var newProject = Projects.newProject(projectTitle);
    
    Projects.save(newProject).then(function successCallback(response) {
      
      console.log(JSON.stringify(response.data));
      
      $scope.projects.push(response.data);
      $scope.selectProject(response.data, $scope.projects.length-1);
      
    }, function errorCallback(response) {
      console.log(JSON.stringify(response));
    });
    
  };
  
  Projects.all().then(function successCallback(response) {
    $scope.projects = response.data;
    
    $scope.activeProject = $scope.projects[$scope.projects.length-1];
    
    $timeout(function() {
      if ($scope.projects.length == 0) {
        while(true) {
          var projectTitle =  prompt('Your first project title: ');
          if (projectTitle) {
            createProject(projectTitle);
            break;
          }
        }
      }
    });
  }, function errorCallback(response) {
    console.log(JSON.stringify(response));
  });
  
  $scope.newProject = function() {
    var projectTitle = prompt('Project name');
    
    if (projectTitle) {
      createProject(projectTitle);
    }
  }
  
  $scope.selectProject = function(project) {
    $scope.activeProject = project;
    $ionicSideMenuDelegate.toggleLeft(false);
  }
  
  $ionicModal.fromTemplateUrl('new-task.html', function(modal) {
    $scope.taskModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
  
  $scope.createTask = function(task) {
    
    if (!$scope.activeProject || !task) {
      return;
    }
    
    $scope.activeProject.tasks.push({title: task.title, project_id: $scope.activeProject.id});
    
    $scope.taskModal.hide();
    
    Projects.save($scope.activeProject).then(function successCallback(response) {
      
      $scope.activeProject = response.data;
      
      var selectedIndex = 0;
      
      $scope.projects.forEach(function(element, index) {
        if (element.id == $scope.activeProject.id) {
          selectedIndex = index;
        }
      }, this);
      
      $scope.projects[selectedIndex] = $scope.activeProject;
      
    }, function errorCallback(response) {
      console.log(JSON.stringify(response));
    });
    
    task.title = "";
  };
  
  $scope.newTask = function() {
    $scope.taskModal.show();
  };
  
  $scope.closeNewTask = function() {
    $scope.taskModal.hide();
  };
  
  $scope.toggleProjects = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };
})


